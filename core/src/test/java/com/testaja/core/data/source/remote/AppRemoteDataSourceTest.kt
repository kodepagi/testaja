//package com.testaja.core.data.source.remote
//
//import kotlinx.coroutines.runBlocking
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
//import org.mockito.MockitoAnnotations
//
//
//class AppRemoteDataSourceTest {
//
//    private lateinit var remoteDataSource: AppRemoteDataSource
//
//    @Before
//    fun setUp() {
//        MockitoAnnotations.initMocks(this)
//        remoteDataSource = AppRemoteDataSource
//    }
//
//
//    @Test
//    fun testGetMovies() {
//        val result = runBlocking {remoteDataSource.getMovies().data ?: emptyList() }
//        assert(result.isNotEmpty())
//    }
//
//
//    @Test
//    fun dummyTest(){
//        val value = 20
//        Assert.assertEquals(40, value + 20)
//    }
//
//
//}