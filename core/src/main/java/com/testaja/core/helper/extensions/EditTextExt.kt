package com.testaja.core.helper.extensions

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.testaja.core.AppEnviroment
import com.testaja.core.R
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


fun EditText.addCurrencyCustomTransportationForm(textView: TextView,
                                                 convert: TextView?,
                                                 switchMaterial: SwitchMaterial?, currency: Double?) {
    this.addTextChangedListener(object: TextWatcher {
        
        @SuppressLint("SetTextI18n")
        override fun afterTextChanged(s: Editable?) {
            try {
                var nominal = getNormalizeNominal(this@addCurrencyCustomTransportationForm.text.toString())
                textView.text = nominal
                if (nominal.isNotEmpty() && "0" == nominal.substring(0, 1)) {
                    this@addCurrencyCustomTransportationForm.removeTextChangedListener(this)
                    this@addCurrencyCustomTransportationForm.setText("")
                    this@addCurrencyCustomTransportationForm.addTextChangedListener(this)
                } else {

                    if (nominal.isBlank()) {
                        switchMaterial?.isEnabled = false
                        switchMaterial?.isChecked = true
                    } else {
                        switchMaterial?.isEnabled = true
                    }

                    this@addCurrencyCustomTransportationForm.removeTextChangedListener(this)
                    this@addCurrencyCustomTransportationForm.setText(getShownNominal(nominal))

                    if (convert != null && nominal.isNotEmpty() && currency != null) {
                        val symbols = DecimalFormatSymbols(Locale.ENGLISH)
                        val format = DecimalFormat("#,###.##", symbols)
                        convert?.text = "$ " + format.format(nominal.toDouble() * (1/currency)).toString()
                    }

                    this@addCurrencyCustomTransportationForm.addTextChangedListener(this)
                    nominal = this@addCurrencyCustomTransportationForm.text.toString()
                    this@addCurrencyCustomTransportationForm.setSelection(nominal.length)
                }
            } catch (e: Exception) {
                Log.e(javaClass.simpleName, e.message)
                this@addCurrencyCustomTransportationForm.removeTextChangedListener(this)
                this@addCurrencyCustomTransportationForm.setText("")
                this@addCurrencyCustomTransportationForm.addTextChangedListener(this)
                throw e
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        
    })   
}

fun getNormalizeNominal(nominal: String): String {
    var nominal = nominal
    return try {
        val a = nominal.replace(" ", "")
        val z = a.replace("-", "")
        val b = z.replace("Rp ", "")
        val c = b.replace("Rp", "")
        val d = c.replace(",00", "")
        nominal = d.replace(".", "")
        nominal
    } catch (e: Exception) {
        Log.e("", e.message)
        nominal
    }
}

fun getShownNominal(nominal: String): String? {
    var nominal = nominal
    var result = ""
    val a = nominal.replace(" ", "")
    var j = ""
    var k = a
    if (a.endsWith(",00")) {
        j = a.substring(0, a.length - 3)
        k = j
    }
    if (a.endsWith(",0")) {
        j = a.substring(0, a.length - 2)
        k = j
    }
    val z = k.replace("-", "")
    val b = z.replace("Rp ", "")
    val c = b.replace("Rp", "")
    val d = c.replace(",00", "")
    nominal = d.replace(",", "")
    var count = 1
    for (i in nominal.length - 1 downTo 0) {
        result = nominal[i].toString() + result
        if (count % 3 == 0 && i != 0) result = ".$result"
        count++
    }
    return "Rp $result"
}

fun TextInputEditText.showDatePickerDialog(textView: TextView, onCancelCallback: () -> Unit = {}) {
    val calendar = Calendar.getInstance()
    val dateFormat = SimpleDateFormat(AppEnviroment.ConstDate.DAY_WITH_DATE_LOCALE_FULL, Locale.US)
    val dateFormatRequest = SimpleDateFormat(AppEnviroment.ConstDate.DATE_LOCALE_DD_MM_YYYY, Locale.US)

    val pickerListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        setText(dateFormat.format(calendar.time))
        textView.text = dateFormatRequest.format(calendar.time)
    }

    val datePickerDialog = DatePickerDialog(
            this.context,
            R.style.TimePickerTheme,
            pickerListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
    ).apply {
        show()
        getButton(DatePickerDialog.BUTTON_NEGATIVE)?.let {
            it.setTextColor(ContextCompat.getColor(this.context, R.color.colorAccent))
            it.setBackgroundColor(ContextCompat.getColor(this.context, android.R.color.transparent))
        }
        getButton(DatePickerDialog.BUTTON_POSITIVE).let {
            it.setTextColor(ContextCompat.getColor(this.context, R.color.mainWhite))
            it.setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorAccent))
        }
    }

    calendar.time?.let { datePickerDialog.datePicker.minDate = calendar.time.time }

    if (calendar.get(Calendar.DAY_OF_MONTH) != -1 && calendar.get(Calendar.MONTH) != -1 && calendar.get(Calendar.YEAR) != -1) {
        datePickerDialog.datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

    datePickerDialog.setOnCancelListener {
        onCancelCallback.invoke()
        datePickerDialog.dismiss()
    }

}

fun TextInputEditText.showTimePickerDialog(textView: TextView, onCancelCallback: () -> Unit = {}) {

    val calendar: Calendar = Calendar.getInstance()
    val hour = calendar[Calendar.HOUR_OF_DAY]
    val minute = calendar[Calendar.MINUTE]

    val pickerListener = TimePickerDialog.OnTimeSetListener { _tp, sHour, sMinute ->
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, sHour)
        calendar.set(Calendar.MINUTE, sMinute)

        val m24 = SimpleDateFormat("HH:mm:ss")
        val m12 = SimpleDateFormat("hh:mm a")
        val time24 = m24.format(calendar.time)
        val time12 = m12.format(calendar.time)

        textView.text = time24
        setText(time12)
    }

    if (this == null) return
    val timePickerDialog = TimePickerDialog(
            this.context,
            R.style.TimePickerTheme,
            pickerListener,
            hour,
            minute,
            false)
            .apply {
                show()
                getButton(DatePickerDialog.BUTTON_NEGATIVE)?.let {
                    it.setTextColor(ContextCompat.getColor(this.context, R.color.colorAccent))
                    it.setBackgroundColor(ContextCompat.getColor(this.context, android.R.color.transparent))
                }
                getButton(DatePickerDialog.BUTTON_POSITIVE).let {
                    it.setTextColor(ContextCompat.getColor(this.context, R.color.mainWhite))
                    it.setBackgroundColor(ContextCompat.getColor(this.context, R.color.colorAccent))
                }
            }

    if (hour != -1 && minute != -1) {
        timePickerDialog.updateTime(hour, minute)
    }

    timePickerDialog.setOnCancelListener {
        onCancelCallback.invoke()
        timePickerDialog.dismiss()
    }

    timePickerDialog.show()
}

fun TextInputLayout.clearError() {
    this.error = null
    this.isErrorEnabled = false
}

