package com.testaja.core.helper.extensions

import android.view.View

fun androidx.recyclerview.widget.RecyclerView.setupLinearLayoutManager(orientation: Int, isReversed: Boolean) {
    layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            if (orientation == 1) androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL else androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            isReversed)
    setHasFixedSize(true)
    itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
    setItemViewCacheSize(30)
    isDrawingCacheEnabled = true
    drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
}

fun androidx.recyclerview.widget.RecyclerView.setupGridLayoutManager(span: Int, orientation: Int, isReversed: Boolean) {
    layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, span)
    setHasFixedSize(true)
    itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
    setItemViewCacheSize(30)
    isDrawingCacheEnabled = true
    drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
}