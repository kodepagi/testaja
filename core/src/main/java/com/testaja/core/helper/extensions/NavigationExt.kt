package com.testaja.core.helper.extensions

import android.content.Context

/**
 *
 * In syaa Allah created & modified
 * by mochadwi on 27/02/19
 * dedicated to build e-nutri
 *
 */

/**
 * Safer ways to use requireContext or requireActivity
 */
fun androidx.fragment.app.Fragment.safeRequireActivity(): androidx.fragment.app.FragmentActivity? = if (isAdded) requireActivity() else activity

fun androidx.fragment.app.Fragment.safeRequireContext(): Context? = if (isAdded) requireContext() else context
