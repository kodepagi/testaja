package com.testaja.core.helper.extensions

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import android.telephony.PhoneNumberUtils
import android.text.Editable
import android.text.Html
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.MutableLiveData
import com.testaja.core.base.BaseApiModel
import com.testaja.core.base.ResultSet
import com.google.gson.Gson
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.regex.Pattern


fun String?.decodeHtml(): String {
    return if (this != null) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT).toString()
        } else {
            Html.fromHtml(this).toString()
        }
    } else {
        ""
    }
}

fun String?.toEditable(): Editable {
    return Editable.Factory.getInstance().newEditable(this ?: "")
}

fun Boolean?.safe(default: Boolean = false): Boolean {
    return this ?: default
}

fun Double?.safe(default: Double = 0.0): Double {
    return this ?: default
}

fun Int?.safe(default: Int = 0): Int {
    return this ?: default
}

fun String?.safe(default: String = ""): String {
    return if (this.isNullOrEmpty()) {
        default
    } else {
        this
    }
}

fun <T> List<T>?.safe(): List<T> {
    return this ?: emptyList()
}


fun <T : MutableList<D>, D> MutableLiveData<T>?.add(data: D) {
    this?.value?.add(data)
    this?.postValue(this.value)
}

fun <T : MutableList<D>, D> MutableLiveData<T>?.clear() {
    this?.value?.clear()
    this?.postValue(this.value)
}

fun <T : MutableList<D>, D> MutableLiveData<T>?.addAll(data: List<D>) {
    this?.value?.addAll(data)
    this?.postValue(this.value)
}


fun <T> BaseApiModel<T>.getResult(): ResultSet<T> {

    return try {
        val data = this
        if (data.code != null && data.code in 200..204 && data.data != null) {
            ResultSet(data.code, true, data.message.safe(), data.data)
        } else {
            ResultSet(data.code?:500, false, data.message.safe(), data.data)
        }

    } catch (e: Exception) {
        e.printStackTrace()
        when (e) {
            is HttpException -> {
                val code = e.code()
                var msg = e.message()
                val baseDao: BaseApiModel<T>?
                try {
                    val body = e.response()?.errorBody()
                    baseDao = Gson().fromJson<BaseApiModel<T>>(body!!.string(), BaseApiModel::class.java)
                } catch (exception: Exception) {
                    return ResultSet(-1, false, exception.message.safe(exception.localizedMessage), null)
                }

                when (code) {
                    504 -> {
                        msg = baseDao?.message ?: "Error Response"
                    }
                    502, 404 -> {
                        msg = baseDao?.message ?: "Error Connect or Resource Not Found"
                    }
                    400 -> {
                        msg = baseDao?.message ?: "Bad Request"
                    }
                    401 -> {
                        msg = baseDao?.message ?: "Not Authorized"
                    }
                    422 -> {
                        msg = baseDao?.message ?: "Unprocessable Entity"
                    }
                }

                return ResultSet(code, false, msg, null)
            }
            is UnknownHostException -> return ResultSet(-1, false, "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}", null)
            is SocketTimeoutException -> return ResultSet(-1, false, "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}", null)
            else -> return ResultSet(-1, false, "Unknown error occured", null)
        }
    }


}

fun Context.getDeviceId(
): String {
    return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
}

fun Double.currencyFormatToRupiah(): String {
    val kursIndonesia = DecimalFormat.getCurrencyInstance() as DecimalFormat
    val formatRp = DecimalFormatSymbols()

    formatRp.currencySymbol = "Rp. "
    formatRp.monetaryDecimalSeparator = '.'
    formatRp.groupingSeparator = ','

    kursIndonesia.decimalFormatSymbols = formatRp
    return kursIndonesia.format(this)
}

fun String.setClearWebviewContent(): String {
    val head = "<head><style>img{max-width: 100%; height: auto;} body { margin: 0; }" +
            "iframe {display: block; background: #000; border-top: 4px solid #000; border-bottom: 4px solid #000;" +
            "top:0;left:0;width:100%;height:235;}</style></head>"
    return "<html>$head<body>$this</body></html>"
}

fun Boolean.isLocaleDate(
        isLocale: Boolean
): Locale {
    return if (isLocale) Locale("id", "ID")
    else Locale("en", "EN")
}

fun String.emailValidate(): Boolean {
    val emailPattern = Pattern.compile(
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
                    "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    )
    return emailPattern.matcher(this).matches()
}

fun Boolean.phoneValidate(
        phone: String
): Boolean {
    return PhoneNumberUtils.isGlobalPhoneNumber(phone)
}

fun Context.isNetworkAvailable(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun Context.getScreenHeight(): Int {
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val dm = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(dm)
    return dm.heightPixels
}

fun Context.getScreenWidth(): Int {
    val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val dm = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(dm)
    return dm.widthPixels
}

fun Context.getStatusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}

fun Context.autofitColumnsGrid(
): Int {
    val displayMetrics = resources.displayMetrics
    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
    return (dpWidth / 180).toInt()
}

fun Int.isSuccessCode(): Boolean {
    return this == 200 || this == 201
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun Activity.hideKeyBoard() {
    val imm =  this@hideKeyBoard .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view: View? = this@hideKeyBoard.getCurrentFocus()
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(this@hideKeyBoard)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}