package com.testaja.core.helper.extensions

import android.graphics.Typeface
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import com.testaja.core.AppEnviroment.ConstOther.SNACKBAR_TIMER_SHOWING_DEFAULT
import com.testaja.core.R

fun View.visible(

) {
    visibility = View.VISIBLE
}

fun View.gone(

) {
    visibility = View.GONE
}

fun View.invisible(

) {
    visibility = View.INVISIBLE
}

fun View.showSnackbarWithCustomColor(
    message: String,
    textColor: Int, backgroundColor: Int,
    duration: Int = 5000
) {

    val finalDuration = if (duration == 0) {
        SNACKBAR_TIMER_SHOWING_DEFAULT
    } else {
        duration
    }

    val finalTextColor = if (textColor == 0) {
        ContextCompat.getColor(this.context, R.color.mainWhite)
    } else {
        textColor
    }

    val finalBackgroundColor = if (textColor == 0) {
        ContextCompat.getColor(this.context, R.color.greyBackgroundDefault)
    } else {
        backgroundColor
    }

    val snackView = Snackbar.make(this, message, finalDuration)
    snackView.setActionTextColor(finalTextColor)
    snackView.view.setBackgroundColor(finalBackgroundColor)
    snackView.show()
}

fun View.showSnackbarDefault(
    message: String,
    duration: Int = 5000
) {
    val finalDuration = if (duration == 0) {
        SNACKBAR_TIMER_SHOWING_DEFAULT
    } else {
        duration
    }

    Snackbar.make(this, message, finalDuration).show()
}

fun View.setCustomFont(
    fontName: String
): Typeface = Typeface
    .createFromAsset(this.context.assets, "fonts/$fontName")


fun androidx.recyclerview.widget.RecyclerView.verticalListStyle(

) {
    layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
    setHasFixedSize(true)
    itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
    setItemViewCacheSize(30)
    isDrawingCacheEnabled = true
    drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
}

fun androidx.recyclerview.widget.RecyclerView.horizontalListStyle(

) {
    layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL, false)
    setHasFixedSize(true)
    itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
    setItemViewCacheSize(30)
    isDrawingCacheEnabled = true
    drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
}

fun View.onClick(clickable: (v: View) -> Unit) = this.setOnClickListener { clickable(this) }

fun NestedScrollView.scrollToBottom() {
    this@scrollToBottom.post(Runnable { this@scrollToBottom.fullScroll(ScrollView.FOCUS_DOWN) })
}