package com.testaja.core.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.android.material.card.MaterialCardView
import com.testaja.core.R
import com.testaja.core.helper.extensions.gone
import com.testaja.core.helper.extensions.onClick
import com.testaja.core.helper.extensions.visible

class SearchBoxLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    private lateinit var etSearch: EditText
    private lateinit var tvSearch: TextView
    private lateinit var rlSearch: RelativeLayout
    private lateinit var ivClear: ImageView

    private lateinit var listener: OnClickListener

    private var isEditText = true
    private var isFocus = false
    private var hintLabel: String? = ""

    init {
        initAttribute(attrs)
        inflateView()
    }

    private fun initAttribute(attrs: AttributeSet?) {
        attrs.let {
            val arr = context.obtainStyledAttributes(it, R.styleable.SearchBoxLayout)
            isEditText = arr.getBoolean(R.styleable.SearchBoxLayout_isEditText, true)
            hintLabel = arr.getString(R.styleable.SearchBoxLayout_hintLabel)
            isFocus = arr.getBoolean(R.styleable.SearchBoxLayout_isEditTextFocus, false)
            arr.recycle()
        }
    }

    private fun inflateView() {
        val root = View.inflate(context, R.layout.view_search_box, this)
        initViews(root)
    }

    private fun initViews(root: View) {
        etSearch = root.findViewById(R.id.et_search)
        tvSearch = root.findViewById(R.id.tv_search)
        rlSearch = root.findViewById(R.id.rl_search)
        ivClear = root.findViewById(R.id.iv_clear_query)

        setUpView()

        setupEvent()
    }

    private fun setUpView() {
        if (!isEditText) {
            etSearch.gone()
            tvSearch.visible()
            tvSearch.hint = hintLabel
        } else {
            etSearch.visible()
            tvSearch.gone()
            etSearch.hint = hintLabel

            if (isFocus)
                etSearch.requestFocus()
        }
    }

    private fun setupEvent() {
        ivClear.onClick {
            etSearch.setText("")
            listener.onClearSearch(it)
        }

        etSearch.onClick {
            listener.onClick(it, "")
        }

        tvSearch.onClick {
            listener.onClick(it, "")
        }

        rlSearch.onClick {
            if (etSearch.text.isNotBlank()) {
                listener.onClick(it, etSearch.text.toString())
            }
        }
    }

    interface OnClickListener {
        fun onClick(view: View, query: String)
        fun onClearSearch(view: View)
    }

    interface OnEditorListener {
        fun onSearchAction(query: String)
    }

    fun setOnClickListener(listener: OnClickListener) {
        this.listener = listener
    }

    fun setTextToSearchBox(query: String) {
        etSearch.setText(query)
    }

    fun setOnEditorActionListener(listener: OnEditorListener) {
        etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                listener.onSearchAction(etSearch.text.toString())
            }
            false
        }
    }
}
