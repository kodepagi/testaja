package com.testaja.core.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity




abstract class BaseActivity : AppCompatActivity() {

    lateinit var mActivity: AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(bindLayoutRes())
        setupToolbar()
        onStartWork()
    }

    abstract fun bindLayoutRes(): View

    abstract fun setupToolbar()

    abstract fun onStartWork()

}