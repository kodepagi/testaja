package com.testaja.core.base

data class ResultSet<T>(
        val code: Int,
        val status: Boolean,
        val message: String,
        val data: T?)