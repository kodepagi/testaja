package com.testaja.core.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.facebook.shimmer.ShimmerFrameLayout
import com.testaja.core.R
import com.testaja.core.base.MessageType.MESSAGE_TYPE_SNACK
import com.testaja.core.base.MessageType.MESSAGE_TYPE_SNACK_CUSTOM
import com.testaja.core.helper.extensions.*


abstract class BaseFragment<T : BaseViewModel> : Fragment() {
    lateinit var mParentVM: T
    private var mMessageType = MESSAGE_TYPE_SNACK

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onCreateInjector()
        mParentVM = onCreateViewModel()
    }

    override fun onViewCreated(paramView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(paramView, savedInstanceState)
        mParentVM.apply {
            eventGlobalMessage.observe(this@BaseFragment, Observer {
                if (it != null) {
                    when (mMessageType) {
                        MESSAGE_TYPE_SNACK_CUSTOM -> {
                            view?.showSnackbarWithCustomColor(it,
                                    R.color.colorAccent,
                                    R.color.greyBackgroundDefault)
                        }
                        MESSAGE_TYPE_SNACK -> {
                            view?.showSnackbarDefault(it)
                        }
                        else -> {
                            requireContext().showToast(it)
                        }
                    }
                }
            })

            eventGlobalMessageInt.observe(this@BaseFragment, Observer {
                if (it != null) {
                    when (mMessageType) {
                        MESSAGE_TYPE_SNACK_CUSTOM -> {
                            view?.showSnackbarWithCustomColor(getString(it),
                                    R.color.colorAccent,
                                    R.color.greyBackgroundDefault)
                        }
                        MESSAGE_TYPE_SNACK -> {
                            view?.showSnackbarDefault(getString(it))
                        }
                        else -> {
                            requireContext().showToast(getString(it))
                        }
                    }
                }
            })
        }


        onCreateObserver(mParentVM)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setContentData()
        mMessageType = getMessageType()
        mParentVM.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        mParentVM.onClearDisposable()
    }

    protected fun shimmerLayout(sfl: ShimmerFrameLayout, content: View, isLoading: Boolean) {
        if (isLoading) {
            if (!sfl.isShimmerStarted) {
                sfl.visible()
                sfl.startShimmer()
                content.invisible()
            }
        } else if (sfl.isShimmerStarted) {
            sfl.stopShimmer()
            sfl.gone()
            content.visible()
        }
    }

    abstract fun onCreateInjector()
    abstract fun onCreateViewModel(): T
    abstract fun onCreateObserver(viewModel: T)
    abstract fun setContentData()
    abstract fun getMessageType(): MessageType

}