package com.testaja.core.base



enum class MessageType {
    MESSAGE_TYPE_TOAST,
    MESSAGE_TYPE_SNACK,
    MESSAGE_TYPE_SNACK_CUSTOM
}