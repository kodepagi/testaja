package com.testaja.core.base

/**
 * Dibuat oleh Irfan Irawan Sukirman
 * @Copyright 2018
 */
interface BaseDataSource {

    interface AppResponseCallback<T> {
        fun onSuccess(data: T)
        fun onFinish()
        fun onFailed(statusCode: Int, errorMessage: String? = "")
    }
}