package com.testaja.core.base

import com.google.gson.annotations.SerializedName



data class BaseApiModel<T>(
        @SerializedName("code") val code: Int?,
        @SerializedName("message") val message: String?,
        @SerializedName("data") val data: T? = null
)