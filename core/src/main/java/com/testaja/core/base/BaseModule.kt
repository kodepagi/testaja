package com.testaja.core.base

import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class BaseModule {

    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

}