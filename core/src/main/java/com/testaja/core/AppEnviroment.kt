package com.testaja.core

import com.testaja.core.AppEnviroment.ConstNetwork.API_KEY


object AppEnviroment {

    object ConstDate {
        const val DATE_TIME_GLOBAL = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //
        const val DATE_TIME_STANDARD = "yyyy-MM-dd HH:mm:ss" // 2018-10-02 12:12:12
        const val DATE_ENGLISH_YYYY_MM_DD = "yyyy-MM-dd" // 2018-10-02
        const val DATE_ENGLISH_YYYY_MM_DD_CLEAR = "yyyy MM dd" // 2018 10 02
        const val DATE_LOCALE_DD_MM_YYYY = "dd-MM-yyyy" // 02-10-2018
        const val DATE_LOCALE_DD_MM_YYYY_CLEAR = "dd MM yyyy" // 02-10-2018
        const val TIME_GENERAL_HH_MM_SS = "HH:mm:ss" // 12:12:12
        const val TIME_GENERAL_HH_MM = "HH:mm" // 12:12
        const val DAY_WITH_DATE_TIME_ENGLISH = "EEE, MMM dd yyyy HH:mm" // Mon, Aug 12 2018 12:12
        const val DAY_WITH_DATE_TIME_LOCALE = "EEE, dd MMM yyyy HH:mm" // Sen, 12 Agt 2019 12:12
        const val DAY_WITH_DATE_TIME_ENGLISH_FULL = "EEEE, MMMM dd yyyy HH:mm" // Monday, August 12 2018 12:12
        const val DAY_WITH_DATE_TIME_LOCALE_FULL = "EEEE, dd MMMM yyyy HH:mm" // Senin, 12 Agustus 2018 12:12
        const val DAY_WITH_DATE_LOCALE_FULL = "EEEE, dd MMMM yyyy" // Senin, 12 Agustus 2018 12:12
    }

    object ConstIntCode {

        const val REQUEST_CODE_GALLERY = 5115

        // Transportation
        const val LOCATION_PERMISSION_REQUEST_CODE = 111
        const val AUTOCOMPLETE_REQUEST_CODE = 511
        const val CODE_LOCATION_PICKUP_RESULT = 5111
        const val CODE_LOCATION_DROP_RESULT = 5112
        const val CODE_LOCATION_STOP_RESULT = 5113

        const val POINT_PICKUP = 1
        const val POINT_DROP = 2
        const val POINT_STOP = 3

    }


    object ConstKey {

        // INTENT KEY
        const val INTENT_EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID"

        // ARGUMENTS KEY
        const val ARGUMENT_MOVIE_ID = "ARGUMENT_MOVIE_ID"

        // PREFERENCES KEY
    }

    object ConstNetwork {

        const val BASE_URL = BuildConfig.BASE_URL
        const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w300/"
        const val API_KEY = BuildConfig.API_KEY

    }

    object ConstFile {
        val APP_FOLDER_DEFAULT = ""
    }

    object ConstOther {
        // TODO add uncategories const in here

        const val APPLICATION_JSON = "application/json"
        const val DB_AUTHENTICATION = "testaja.db"

        val HTTP_STRING = "http"

        val SNACKBAR_TIMER_SHOWING_DEFAULT = 2000
    }

}