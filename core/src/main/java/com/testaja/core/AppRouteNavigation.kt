package com.testaja.core

import android.content.Context
import com.testaja.core.helper.extensions.navigatorImplicit


object AppRouteNavigation {

    const val PACKAGE_MAIN_PAGE = "com.testaja.android.ui.main.MainActivity"

    fun openMainPage(context: Context) {
        context.navigatorImplicit(context.packageName, PACKAGE_MAIN_PAGE)
    }

}

