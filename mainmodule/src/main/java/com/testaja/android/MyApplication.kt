package com.testaja.android

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this

        // Debug
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    companion object {
        lateinit var instance: MyApplication

        fun getContext(): Context = instance.applicationContext
    }
}