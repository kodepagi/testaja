package com.testaja.android.ui.detail

import androidx.lifecycle.ViewModel
import com.testaja.android.di.MainScope
import com.testaja.core.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailModule {

    @MainScope
    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun bindDetailViewModel(viewModel: DetailViewModel): ViewModel

}
