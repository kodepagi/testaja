package com.testaja.android.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.testaja.android.R
import com.testaja.android.databinding.FragmentDetailBinding
import com.testaja.android.di.DaggerMainComponent
import com.testaja.android.di.MainDataModule
import com.testaja.core.AppEnviroment
import com.testaja.core.AppEnviroment.ConstKey.ARGUMENT_MOVIE_ID
import com.testaja.core.base.BaseFragment
import com.testaja.core.base.MessageType
import com.testaja.core.helper.extensions.onClick
import com.testaja.core.helper.extensions.putArgs
import com.testaja.core.helper.extensions.showToast
import com.testaja.core.helper.other.GlideApp
import javax.inject.Inject


class DetailFragment : BaseFragment<DetailViewModel>() {

    private var _binding: FragmentDetailBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var movieId = 0
    private var isFavourite = false

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: DetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateInjector() {
        DaggerMainComponent.builder()
            .mainDataModule(MainDataModule(requireContext()))
            .build().inject(this@DetailFragment)
    }

    override fun onCreateViewModel(): DetailViewModel {
        return mViewModel
    }

    override fun onCreateObserver(viewModel: DetailViewModel) {
        viewModel.apply {
            eventShowProgress.observe(this@DetailFragment, Observer {
                shimmerLayout(binding.shimmerFrameLayout1, binding.clDetail, it)
            })

            movieDetailResponse.observe(this@DetailFragment, Observer {
                GlideApp.with(requireContext())
                    .load("${AppEnviroment.ConstNetwork.BASE_URL_IMAGE}${it.posterPath}")
                    .into(binding.ivPoster)
                binding.tvTitle.text = it.title
                binding.tvOverview.text = it.overview
                binding.tvVoteAverage.text = it.voteAverage.toString()
                binding.tvRelease.text = "${"Relase Date: "}${it.releaseDate}"
            })

            favorites.observe(this@DetailFragment, Observer {
                it.forEach { favorite ->
                    if (movieId == favorite.id) {
                        isFavourite = true
                        binding.ivFavourite.setImageDrawable(
                            ContextCompat.getDrawable(
                                requireContext(),
                                R.drawable.ic_baseline_favorite_24
                            )
                        )
                    }
                }
            })

            eventSaveMovie.observe(this@DetailFragment, Observer {
                if (it) {
                    showToast("Added to favourite")
                    isFavourite = true
                    binding.ivFavourite.setImageDrawable(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_baseline_favorite_24
                        )
                    )
                } else {
                    showToast("Failed")
                }
            })

            eventDeleteMovie.observe(this@DetailFragment, Observer {
                if (it) {
                    showToast("Deleted from favourite")
                    binding.ivFavourite.setImageDrawable(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.ic_baseline_favorite_24_white
                        )
                    )
                } else {
                    showToast("Failed")
                }
            })
        }
    }

    override fun setContentData() {
        movieId = arguments?.getInt(ARGUMENT_MOVIE_ID) ?: 0
        mViewModel.getMovie(movieId)

        binding.ivFavourite.onClick {
            if (isFavourite) {
                mViewModel.deleteFavourite(movieId)
            } else {
                mViewModel.movieDetailResponse.value?.let { mViewModel.saveFavourite(it) }
            }
        }
    }

    override fun getMessageType(): MessageType {
        return MessageType.MESSAGE_TYPE_SNACK
    }

    companion object {
        fun newInstance(movieId: Int) = DetailFragment().putArgs {
            putInt(ARGUMENT_MOVIE_ID, movieId)
        }
    }
}
