package com.testaja.android.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testaja.android.databinding.ItemMovieBinding
import com.testaja.core.AppEnviroment
import com.testaja.core.helper.extensions.onClick
import com.testaja.core.helper.other.GlideApp
import com.testaja.db.model.MovieModel

class MovieAdapter(
    private val context: Context,
    private val movieActionListener: MovieActionListener
) : RecyclerView.Adapter<MovieAdapter.MovieHolder>() {

    private val movies = ArrayList<MovieModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val itemBinding =
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        val movie: MovieModel = movies[position]
        holder.bind(context, movie, movieActionListener)
    }

    override fun getItemCount() = movies.size

    fun setMovies(movies: List<MovieModel>?) {
        movies?.let {
            this.movies.apply {
                clear()
                addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    fun clear() {
        movies.clear()
        notifyDataSetChanged()
    }

    fun getMovies(): List<MovieModel> {
        return movies
    }

    class MovieHolder(private val itemBinding: ItemMovieBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(context: Context, movie: MovieModel, movieActionListener: MovieActionListener) {

            itemBinding.apply {
                GlideApp.with(context)
                    .load("${AppEnviroment.ConstNetwork.BASE_URL_IMAGE}${movie.posterPath}")
                    .into(ivPoster)
                tvTitle.text = movie.title
                tvVoteAverage.text = movie.voteAverage.toString()
                tvOverview.text = movie.overview
                tvRelease.text = movie.releaseDate
                root.onClick {
                    movieActionListener.onMovieClicked(movie.id)
                }
            }
        }
    }
}
