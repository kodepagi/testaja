package com.testaja.android.ui.main

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.testaja.android.R
import com.testaja.core.base.BaseActivity
import com.testaja.android.databinding.ActivityMainBinding
import com.testaja.android.ui.favourite.FavouriteActivity
import com.testaja.android.ui.search.SearchActivity
import com.testaja.android.ui.splash.SplashFragment

class MainActivity: BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun bindLayoutRes(): View {
        binding = ActivityMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.txtToolbarTitle.text = getString(R.string.toolbar_title_movies)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_search -> {
                val intent = Intent(this, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.menu_favourite -> {
                val intent = Intent(this, FavouriteActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStartWork() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_main, MainFragment())
        }.commit()
    }

}
