package com.testaja.android.ui.detail

import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.domain.MainUseCase
import com.testaja.core.base.BaseViewModel
import com.testaja.core.helper.other.SingleLiveEvent
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.MovieModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DetailViewModel
@Inject
constructor() : BaseViewModel() {

    @Inject
    lateinit var mUseCase: MainUseCase

    val movieDetailResponse = SingleLiveEvent<MovieDetailResponse>()
    val eventSaveMovie = SingleLiveEvent<Boolean>()
    val eventDeleteMovie = SingleLiveEvent<Boolean>()
    val favorites = SingleLiveEvent<List<MovieModel>>()

    fun getMovie(movieId: Int) {
        eventShowProgress.value = true
        launch {
            withContext(dispatcher) {
                val content = mUseCase.getMovieDetail(movieId)
                if (content != null) {
                    movieDetailResponse.value = content
                    eventShowProgress.value = false
                    getFavourites()
                } else {
                    eventShowProgress.value = false
                }
            }
        }
    }

    fun saveFavourite(movie: MovieDetailResponse) {
        var favouriteModel = FavouriteModel(
            movie.id ?: 0,
            movie.title,
            movie.posterPath,
            movie.overview,
            movie.voteAverage,
            movie.releaseDate
        )
        launch {
            withContext(dispatcher) {
                val content = mUseCase.saveFavourite(favouriteModel)
                eventSaveMovie.value = content == "success"
            }
        }
    }

    fun getFavourites() {
        launch {
            withContext(dispatcher) {
                val content = mUseCase.getFavourites()
                if (content != null) {
                    favorites.value = content
                }
            }
        }
    }

    fun deleteFavourite(movieId: Int) {
        launch {
            withContext(dispatcher) {
                val content = mUseCase.deleteFavourite(movieId)
                eventDeleteMovie.value = content == "success"
            }
        }
    }

}
