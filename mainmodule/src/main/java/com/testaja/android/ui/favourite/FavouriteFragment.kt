package com.testaja.android.ui.favourite

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.testaja.android.databinding.FragmentFavouriteBinding
import com.testaja.android.di.DaggerMainComponent
import com.testaja.android.di.MainDataModule
import com.testaja.android.ui.detail.DetailActivity
import com.testaja.android.ui.main.MovieAdapter
import com.testaja.android.ui.main.MovieActionListener
import com.testaja.core.AppEnviroment
import com.testaja.core.base.BaseFragment
import com.testaja.core.base.MessageType
import javax.inject.Inject


class FavouriteFragment : BaseFragment<FavouriteViewModel>(), MovieActionListener {

    private var _binding: FragmentFavouriteBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var movieAdapter: MovieAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: FavouriteViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(FavouriteViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFavouriteBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateInjector() {
        DaggerMainComponent.builder()
            .mainDataModule(MainDataModule(requireContext()))
            .build().inject(this@FavouriteFragment)
    }

    override fun onCreateViewModel(): FavouriteViewModel {
        return mViewModel
    }

    override fun onCreateObserver(viewModel: FavouriteViewModel) {
        viewModel.apply {
            movies.observe(this@FavouriteFragment, Observer {
                movieAdapter?.setMovies(it)
            })
        }
    }

    override fun setContentData() {
        movieAdapter = MovieAdapter(requireContext(), this)
        binding.rvFavourite.layoutManager = LinearLayoutManager(requireContext())
        binding.rvFavourite.adapter = movieAdapter

        mViewModel.getFavourites()
    }

    override fun getMessageType(): MessageType {
        return MessageType.MESSAGE_TYPE_SNACK
    }

    override fun onMovieClicked(id: Int?) {
        val intent = Intent(requireActivity(), DetailActivity::class.java)
        intent.putExtra(AppEnviroment.ConstKey.INTENT_EXTRA_MOVIE_ID, id)
        startActivity(intent)
    }

}
