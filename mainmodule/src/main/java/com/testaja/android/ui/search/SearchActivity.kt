package com.testaja.android.ui.search

import android.view.MenuItem
import android.view.View
import com.testaja.android.R
import com.testaja.android.databinding.ActivitySearchBinding
import com.testaja.android.databinding.ActivitySplashBinding
import com.testaja.core.base.BaseActivity

class SearchActivity: BaseActivity() {

    private lateinit var binding: ActivitySearchBinding

    override fun bindLayoutRes(): View {
        binding = ActivitySearchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.txtToolbarTitle.text = getString(R.string.toolbar_title_search)
        supportActionBar?.also {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_close_toolbar)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStartWork() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_search, SearchFragment())
        }.commit()
    }
}
