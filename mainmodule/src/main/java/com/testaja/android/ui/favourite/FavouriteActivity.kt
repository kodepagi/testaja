package com.testaja.android.ui.favourite

import android.view.MenuItem
import android.view.View
import com.testaja.android.R
import com.testaja.android.databinding.ActivityFavouriteBinding
import com.testaja.core.base.BaseActivity

class FavouriteActivity: BaseActivity() {

    private lateinit var binding: ActivityFavouriteBinding

    override fun bindLayoutRes(): View {
        binding = ActivityFavouriteBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.txtToolbarTitle.text = getString(R.string.toolbar_title_favourites)
        supportActionBar?.also {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_close_toolbar)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStartWork() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_favourite, FavouriteFragment())
        }.commit()
    }
}
