package com.testaja.android.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.testaja.android.databinding.FragmentSearchBinding
import com.testaja.android.di.DaggerMainComponent
import com.testaja.android.di.MainDataModule
import com.testaja.android.ui.detail.DetailActivity
import com.testaja.android.ui.main.MovieAdapter
import com.testaja.android.ui.main.MovieActionListener
import com.testaja.core.AppEnviroment.ConstKey.INTENT_EXTRA_MOVIE_ID
import com.testaja.core.base.BaseFragment
import com.testaja.core.base.MessageType
import com.testaja.core.helper.extensions.hideKeyBoard
import com.testaja.core.widget.SearchBoxLayout
import javax.inject.Inject


class SearchFragment : BaseFragment<SearchViewModel>(), SearchBoxLayout.OnClickListener,
    MovieActionListener {

    private var _binding: FragmentSearchBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var movieAdapter: MovieAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: SearchViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateInjector() {
        DaggerMainComponent.builder()
            .mainDataModule(MainDataModule(requireContext()))
            .build().inject(this@SearchFragment)
    }

    override fun onCreateViewModel(): SearchViewModel {
        return mViewModel
    }

    override fun onCreateObserver(viewModel: SearchViewModel) {
        viewModel.apply {
            eventShowProgress.observe(this@SearchFragment, Observer {
                shimmerLayout(binding.shimmerFrameLayout2, binding.rvMovie, it)
            })
            movies.observe(this@SearchFragment, Observer {
                movieAdapter?.setMovies(it)
            })
        }
    }

    override fun setContentData() {
        binding.sblMovie.setOnClickListener(this)

        movieAdapter = MovieAdapter(requireContext(), this)
        binding.rvMovie.layoutManager = LinearLayoutManager(requireContext())
        binding.rvMovie.adapter = movieAdapter

    }

    override fun getMessageType(): MessageType {
        return MessageType.MESSAGE_TYPE_SNACK
    }

    override fun onClick(view: View, query: String) {
        if (query.isNotBlank()) {
            requireActivity().hideKeyBoard()
            mViewModel.searchMovies(query)
        }
    }

    override fun onClearSearch(view: View) {

    }

    override fun onMovieClicked(id: Int?) {
        val intent = Intent(requireActivity(), DetailActivity::class.java)
        intent.putExtra(INTENT_EXTRA_MOVIE_ID, id)
        startActivity(intent)
    }

}
