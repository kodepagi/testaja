package com.testaja.android.ui.main

import com.testaja.android.domain.MainUseCase
import com.testaja.core.base.BaseViewModel
import com.testaja.core.helper.other.SingleLiveEvent
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class MainViewModel
@Inject
constructor() : BaseViewModel() {

    @Inject
    lateinit var mUseCase: MainUseCase

    val genres = SingleLiveEvent<List<GenreModel>>()
    val movies = SingleLiveEvent<List<MovieModel>>()
    val eventShowProgressMovie = SingleLiveEvent<Boolean>()

    fun getGenres() {
        eventShowProgress.value = true
        launch {
            withContext(dispatcher) {
                val content = mUseCase.getGenres()
                genres.value = content
                if (content.isNotEmpty()) {
                    getMovies(content[0].id ?: 28)
                } else {
                    eventShowProgress.value = false
                }
            }
        }
    }

    fun getMovies(genre: Int) {
        eventShowProgressMovie.value = true
        launch {
            withContext(dispatcher) {
                val content = mUseCase.getMovies(genre)
                movies.value = content
                eventShowProgress.value = false
                eventShowProgressMovie.value = false
            }
        }
    }

}
