package com.testaja.android.ui.detail

import android.view.MenuItem
import android.view.View
import com.testaja.android.R
import com.testaja.android.databinding.ActivityDetailBinding
import com.testaja.core.AppEnviroment.ConstKey.INTENT_EXTRA_MOVIE_ID
import com.testaja.core.base.BaseActivity
import com.testaja.core.helper.extensions.replaceFragmentInActivity

class DetailActivity: BaseActivity() {

    private lateinit var binding: ActivityDetailBinding
    private var movieId = 0

    override fun bindLayoutRes(): View {
        binding = ActivityDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.txtToolbarTitle.text = getString(R.string.toolbar_title_detail)
        supportActionBar?.also {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_close_toolbar)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStartWork() {
        movieId = intent.getIntExtra(INTENT_EXTRA_MOVIE_ID, 0)

        supportFragmentManager.findFragmentById(R.id.frame_detail)
        DetailFragment.newInstance(movieId).let {
            replaceFragmentInActivity(it, R.id.frame_detail)
        }
    }
}
