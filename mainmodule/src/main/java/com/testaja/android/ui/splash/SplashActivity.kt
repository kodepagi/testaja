package com.testaja.android.ui.splash

import android.view.View
import com.testaja.android.R
import com.testaja.android.databinding.ActivitySplashBinding
import com.testaja.core.base.BaseActivity

class SplashActivity: BaseActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun bindLayoutRes(): View {
        binding = ActivitySplashBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun setupToolbar() {}

    override fun onStartWork() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frame_splash, SplashFragment())
        }.commit()
    }
}
