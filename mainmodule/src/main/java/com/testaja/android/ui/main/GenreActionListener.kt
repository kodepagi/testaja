package com.testaja.android.ui.main

interface GenreActionListener {

    fun onGenreClicked(id: Int)

}