package com.testaja.android.ui.splash

import androidx.lifecycle.ViewModel
import com.testaja.android.di.MainScope
import com.testaja.core.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SplashModule {

    @MainScope
    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

}
