package com.testaja.android.ui.favourite

import com.testaja.android.domain.MainUseCase
import com.testaja.core.base.BaseViewModel
import com.testaja.core.helper.other.SingleLiveEvent
import com.testaja.db.model.MovieModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FavouriteViewModel
@Inject
constructor() : BaseViewModel() {

    @Inject
    lateinit var mUseCase: MainUseCase

    val movies = SingleLiveEvent<List<MovieModel>>()

    fun getFavourites() {
        launch {
            withContext(dispatcher) {
                val content = mUseCase.getFavourites()
                if (content != null) {
                    movies.value = content
                }
            }
        }
    }

}
