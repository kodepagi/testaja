package com.testaja.android.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.testaja.android.databinding.FragmentSplashBinding
import com.testaja.android.di.DaggerMainComponent
import com.testaja.android.di.MainDataModule
import com.testaja.core.base.BaseFragment
import com.testaja.core.base.MessageType
import com.testaja.core.helper.extensions.visible
import javax.inject.Inject
import androidx.lifecycle.Observer
import com.testaja.core.AppRouteNavigation


class SplashFragment : BaseFragment<SplashViewModel>() {

    private var _binding: FragmentSplashBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: SplashViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateInjector() {
        DaggerMainComponent.builder()
                .mainDataModule(MainDataModule(requireContext()))
                .build().inject(this@SplashFragment)
    }

    override fun onCreateViewModel(): SplashViewModel {
        return mViewModel
    }

    override fun onCreateObserver(viewModel: SplashViewModel) {
        viewModel.apply {
            splashSuccess.observe(this@SplashFragment, Observer {
                    AppRouteNavigation.openMainPage(requireActivity())
                    requireActivity().finish()

            })
        }
    }

    override fun setContentData() {
        binding.progress.visible()
        mViewModel.processSplash()
    }

    override fun getMessageType(): MessageType {
        return MessageType.MESSAGE_TYPE_SNACK
    }

}
