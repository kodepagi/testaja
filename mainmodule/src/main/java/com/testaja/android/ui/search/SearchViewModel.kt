package com.testaja.android.ui.search

import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.android.domain.MainUseCase
import com.testaja.core.base.BaseViewModel
import com.testaja.core.helper.other.SingleLiveEvent
import com.testaja.db.model.MovieModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchViewModel
@Inject
constructor() : BaseViewModel() {

    @Inject
    lateinit var mUseCase: MainUseCase

    val movies = SingleLiveEvent<List<MovieModel>>()

    fun searchMovies(query: String) {
        eventShowProgress.value = true
        launch {
            withContext(dispatcher) {
                val content = mUseCase.searchMovies(query)
                movies.value = content
                eventShowProgress.value = false
            }
        }
    }

}
