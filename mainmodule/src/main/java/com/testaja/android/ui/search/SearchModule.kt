package com.testaja.android.ui.search

import androidx.lifecycle.ViewModel
import com.testaja.android.di.MainScope
import com.testaja.core.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SearchModule {

    @MainScope
    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SearchViewModel): ViewModel

}
