package com.testaja.android.ui.splash

import android.os.Handler
import com.testaja.core.base.BaseViewModel
import com.testaja.core.helper.other.SingleLiveEvent
import javax.inject.Inject

class SplashViewModel
@Inject
constructor() : BaseViewModel() {

    val splashSuccess = SingleLiveEvent<Void>()

    fun processSplash() {
        Handler().postDelayed({
            splashSuccess.call()
        }, 3000)
    }
}
