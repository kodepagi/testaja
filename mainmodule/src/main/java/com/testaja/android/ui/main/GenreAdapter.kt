package com.testaja.android.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.testaja.android.databinding.ItemGenreBinding
import com.testaja.core.helper.extensions.onClick
import com.testaja.db.model.GenreModel

class GenreAdapter(
    private val context: Context,
    private val genreActionListener: GenreActionListener
) : RecyclerView.Adapter<GenreAdapter.GenreHolder>() {

    private val genres = ArrayList<GenreModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreHolder {
        val itemBinding =
            ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: GenreHolder, position: Int) {
        val genre: GenreModel = genres[position]
        holder.bind(context, genre, genreActionListener)
    }

    override fun getItemCount() = genres.size

    fun setGenres(genres: List<GenreModel>?) {
        genres?.let {
            this.genres.apply {
                clear()
                addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    fun clear() {
        genres.clear()
        notifyDataSetChanged()
    }

    fun getGenres(): List<GenreModel> {
        return genres
    }

    class GenreHolder(private val itemBinding: ItemGenreBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(context: Context, genre: GenreModel, genreActionListener: GenreActionListener) {

            itemBinding.apply {
                tvTitle.text = genre.name
                root.onClick {
                    genreActionListener.onGenreClicked(genre.id ?: 28)
                }
            }
        }
    }
}
