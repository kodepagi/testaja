package com.testaja.android.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.synnapps.carouselview.ImageListener
import com.testaja.android.R
import com.testaja.android.databinding.FragmentMainBinding
import com.testaja.android.di.DaggerMainComponent
import com.testaja.android.di.MainDataModule
import com.testaja.android.ui.detail.DetailActivity
import com.testaja.core.AppEnviroment.ConstKey.INTENT_EXTRA_MOVIE_ID
import com.testaja.core.base.BaseFragment
import com.testaja.core.base.MessageType
import javax.inject.Inject


class MainFragment : BaseFragment<MainViewModel>(), MovieActionListener, GenreActionListener {

    private var _binding: FragmentMainBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    var sampleImages = intArrayOf(
        R.drawable.carousel1,
        R.drawable.carousel2,
        R.drawable.carousel3
    )
    private var genreAdapter: GenreAdapter? = null
    private var movieAdapter: MovieAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onCreateInjector() {
        DaggerMainComponent.builder()
            .mainDataModule(MainDataModule(requireContext()))
            .build().inject(this@MainFragment)
    }

    override fun onCreateViewModel(): MainViewModel {
        return mViewModel
    }

    override fun onCreateObserver(viewModel: MainViewModel) {
        viewModel.apply {
            eventShowProgress.observe(this@MainFragment, Observer {
                shimmerLayout(binding.shimmerFrameLayout1, binding.rvGenre, it)
            })

            eventShowProgressMovie.observe(this@MainFragment, Observer {
                shimmerLayout(binding.shimmerFrameLayout2, binding.rvMovie, it)
            })

            genres.observe(this@MainFragment, Observer {
                genreAdapter?.setGenres(it)
            })
            movies.observe(this@MainFragment, Observer {
                movieAdapter?.setMovies(it)
            })
        }
    }

    override fun setContentData() {
        binding.crvMain.pageCount = 3
        binding.crvMain.setImageListener(imageListener)

        genreAdapter = GenreAdapter(requireContext(), this)
        binding.rvGenre.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        binding.rvGenre.adapter = genreAdapter

        movieAdapter = MovieAdapter(requireContext(), this)
        binding.rvMovie.layoutManager = LinearLayoutManager(requireContext())
        binding.rvMovie.adapter = movieAdapter

        mViewModel.getGenres()
    }

    var imageListener =
        ImageListener { position, imageView -> imageView.setImageResource(sampleImages.get(position)) }

    override fun getMessageType(): MessageType {
        return MessageType.MESSAGE_TYPE_SNACK
    }

    override fun onMovieClicked(id: Int?) {
        val intent = Intent(requireActivity(), DetailActivity::class.java)
        intent.putExtra(INTENT_EXTRA_MOVIE_ID, id)
        startActivity(intent)
    }

    override fun onGenreClicked(id: Int) {
        mViewModel.getMovies(id)
    }

}
