package com.testaja.android.ui.main

interface MovieActionListener {

    fun onMovieClicked(id: Int?)

}