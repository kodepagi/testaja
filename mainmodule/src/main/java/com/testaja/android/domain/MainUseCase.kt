package com.testaja.android.domain

import android.content.Context
import com.testaja.android.data.MainRepository
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.core.helpe.AppHelper
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel


class MainUseCase(private val context: Context, private val repository: MainRepository) {

    // remote

    suspend fun getGenres(): List<GenreModel> {
        val result = ArrayList<GenreModel>()
        return if (AppHelper.Func.isNetworkAvailable(context) == true) {
            val response = repository.getGenres()
            response?.genres?.let {
                result.addAll(it)
                saveGenres(it)
            }
            result
        } else {
            val response = repository.getGenresLocal()
            response?.let {
                result.addAll(it)
            }
            result
        }
    }

    suspend fun getMovies(genre: Int): List<MovieModel> {
        val result = ArrayList<MovieModel>()
        return if (AppHelper.Func.isNetworkAvailable(context) == true) {
            val response = repository.getMovies(genre)
            response?.resultMovies?.let {
                it.forEach { resultMovie ->
                    val genreId = if (resultMovie.genreIds?.isNotEmpty() == true) {
                        resultMovie.genreIds[0]
                    } else {
                        0
                    }
                    result.add(
                        MovieModel(
                            resultMovie.id ?: 0,
                            resultMovie.title,
                            resultMovie.posterPath,
                            resultMovie.overview,
                            resultMovie.voteAverage,
                            resultMovie.releaseDate,
                            genreId
                        )
                    )
                }
                saveMovies(result)
            }
            result
        } else {
            val response = repository.getMoviesLocal(genre)
            response?.let {
                result.addAll(it)
            }
            result
        }
    }

    suspend fun searchMovies(query: String): List<MovieModel> {
        val result = ArrayList<MovieModel>()
        return if (AppHelper.Func.isNetworkAvailable(context) == true) {
            val response = repository.searchMovies(query)
            response?.resultMovies?.let {
                it.forEach { resultMovie ->
                    val genreId = if (resultMovie.genreIds?.isNotEmpty() == true) {
                        resultMovie.genreIds[0]
                    } else {
                        0
                    }
                    result.add(
                        MovieModel(
                            resultMovie.id ?: 0,
                            resultMovie.title,
                            resultMovie.posterPath,
                            resultMovie.overview,
                            resultMovie.voteAverage,
                            resultMovie.releaseDate,
                            genreId
                        )
                    )
                }
            }
            result
        } else {
            result
        }
    }

    suspend fun getMovieDetail(movieId: Int): MovieDetailResponse? {
        return repository.getMovieDetail(movieId)
    }

    // local

    suspend fun saveGenres(genre: List<GenreModel>): String {
        return repository.saveGenres(genre)
    }

    suspend fun saveMovies(movies: List<MovieModel>): String {
        return repository.saveMovies(movies)
    }

    suspend fun saveFavourite(favouriteModel: FavouriteModel): String {
        return repository.saveFavourite(favouriteModel)
    }

    suspend fun getFavourites(): List<MovieModel>? {
        val result = repository.getFavourites()
        val movies = ArrayList<MovieModel>()
        result?.let {
            it.forEach { resultMovie ->
                movies.add(
                    MovieModel(
                        resultMovie.id,
                        resultMovie.title,
                        resultMovie.posterPath,
                        resultMovie.overview,
                        resultMovie.voteAverage,
                        resultMovie.releaseDate,
                        null
                    )
                )
            }
        }
        return movies
    }

    suspend fun deleteFavourite(movieId: Int): String {
        return repository.deleteFavourite(movieId)
    }
}