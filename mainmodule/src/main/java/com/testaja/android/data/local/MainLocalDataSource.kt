package com.testaja.android.data.local

import android.content.Context
import com.testaja.android.data.MainRepository
import com.testaja.android.data.remote.model.GenreResponse
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.db.AppDatabase
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel

class MainLocalDataSource(val mContext: Context, val mAppDatabase: AppDatabase) : MainRepository {

    override suspend fun getGenres(): GenreResponse? {
        throw Exception("only available on remotedatasource")
    }

    override suspend fun getMovies(genre: Int): MovieResponse? {
        throw Exception("only available on remotedatasource")
    }

    override suspend fun searchMovies(query: String): MovieResponse? {
        throw Exception("only available on remotedatasource")
    }

    override suspend fun getMovieDetail(movieId: Int): MovieDetailResponse? {
        throw Exception("only available on remotedatasource")
    }

    override suspend fun saveGenres(genre: List<GenreModel>): String {
        return try {
            mAppDatabase.genreDao().insertData(genre)
            return "success"
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    override suspend fun getGenresLocal(): List<GenreModel>? {
        return try {
            mAppDatabase.genreDao().getData()
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun saveMovies(movies: List<MovieModel>): String {
        return try {
            mAppDatabase.movieDao().insertData(movies)
            return "success"
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    override suspend fun getMoviesLocal(genre: Int): List<MovieModel>? {
        return try {
            mAppDatabase.movieDao().getData(genre)
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun saveFavourite(favouriteModel: FavouriteModel): String {
        return try {
            mAppDatabase.favouriteDao().insertData(favouriteModel)
            return "success"
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    override suspend fun getFavourites(): List<FavouriteModel>? {
        return try {
            mAppDatabase.favouriteDao().getData()
        } catch (e: Exception) {
            null
        }
    }

    override suspend fun deleteFavourite(movieId: Int): String {
        return try {
            mAppDatabase.favouriteDao().deleteData(movieId)
            return "success"
        } catch (e: Exception) {
            e.localizedMessage
        }
    }
}