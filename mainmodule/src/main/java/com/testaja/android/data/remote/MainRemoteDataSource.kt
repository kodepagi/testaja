package com.testaja.android.data.remote

import com.google.gson.Gson
import com.testaja.android.data.MainRepository
import com.testaja.android.data.remote.model.GenreResponse
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.core.base.BaseApiModel
import com.testaja.core.base.ResultSet
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


class MainRemoteDataSource(private val service: MainService) : MainRepository {

    override suspend fun getGenres(): GenreResponse? {
        return try {
            return service.getGenres()
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun getMovies(genre: Int): MovieResponse? {
        return try {
            return service.getMovies(genre = genre)
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun searchMovies(query: String): MovieResponse? {
        return try {
            return service.searchMovies(query = query)
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun getMovieDetail(movieId: Int): MovieDetailResponse? {
        return try {
            return service.getMovieDetail(movieId = movieId)
        } catch (e: Exception) {
            return null
        }
    }

    override suspend fun saveGenres(genre: List<GenreModel>): String {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun getGenresLocal(): List<GenreModel>? {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun saveMovies(genre: List<MovieModel>): String {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun getMoviesLocal(genre: Int): List<MovieModel>? {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun saveFavourite(favouriteModel: FavouriteModel): String {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun getFavourites(): List<FavouriteModel>? {
        throw Exception("This only available on localdatasource")
    }

    override suspend fun deleteFavourite(movieId: Int): String {
        throw Exception("This only available on localdatasource")
    }

    private fun <T> getExceptionResponse(e: Exception): ResultSet<T> {
        e.printStackTrace()
        when (e) {
            is HttpException -> {
                val code = e.code()
                var msg = e.message()
                val baseDao: BaseApiModel<T?>?
                try {
                    val body = e.response()?.errorBody()
                    baseDao =
                        Gson().fromJson<BaseApiModel<T?>>(body!!.string(), BaseApiModel::class.java)
                } catch (exception: java.lang.Exception) {
                    return when (exception) {
                        is UnknownHostException -> ResultSet(
                            code,
                            false,
                            "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}",
                            null
                        )
                        is SocketTimeoutException -> ResultSet(
                            code,
                            false,
                            "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}",
                            null
                        )
                        else -> ResultSet(
                            code,
                            false,
                            "Terjadi kesalahan pada server. errorcode : <b>$code</b>",
                            null
                        )
                    }
                }

                when (code) {
                    504 -> {
                        msg = baseDao?.message ?: "Error Response"
                    }
                    502, 404 -> {
                        msg = baseDao?.message ?: "Error Connect or Resource Not Found"
                    }
                    400 -> {
                        msg = baseDao?.message ?: "Bad Request"
                    }
                    401 -> {
                        msg = baseDao?.message ?: "Not Authorized"
                    }
                    422 -> {
                        msg = baseDao?.message ?: "Unprocessable Entity"
                    }
                }

                return ResultSet(code, false, msg, null)
            }
            is UnknownHostException -> return ResultSet(
                -1,
                false,
                "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}",
                null
            )
            is SocketTimeoutException -> return ResultSet(
                -1,
                false,
                "Telah terjadi kesalahan ketika koneksi ke server: ${e.message}",
                null
            )
            else -> return ResultSet(-1, false, "Unknown error occured", null)
        }
    }

}