package com.testaja.android.data.remote

import android.content.Context
import com.readystatesoftware.chuck.ChuckInterceptor
import com.testaja.android.data.remote.model.GenreResponse
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.core.AppEnviroment
import com.testaja.core.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface MainService {

    @GET("genre/movie/list")
    suspend fun getGenres(
        @Query("api_key") api_key: String = AppEnviroment.ConstNetwork.API_KEY
    ): GenreResponse?

    @GET("discover/movie")
    suspend fun getMovies(
        @Query("api_key") api_key: String = AppEnviroment.ConstNetwork.API_KEY,
        @Query("with_genres") genre: Int
    ): MovieResponse?

    @GET("search/movie")
    suspend fun searchMovies(
        @Query("api_key") api_key: String = AppEnviroment.ConstNetwork.API_KEY,
        @Query("query") query: String
    ): MovieResponse?

    @GET("movie/{id}")
    suspend fun getMovieDetail(
        @Path("id") movieId: Int,
        @Query("api_key") api_key: String = AppEnviroment.ConstNetwork.API_KEY
    ): MovieDetailResponse?

    companion object Factory {

        fun request(context: Context): Retrofit {
            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val mClient = if (BuildConfig.DEBUG) {
                OkHttpClient.Builder()
                    .addInterceptor(mLoggingInterceptor)
                    .addInterceptor(ChuckInterceptor(context))
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .build()
            } else {
                OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .build()
            }

            return Retrofit.Builder()
                .baseUrl(AppEnviroment.ConstNetwork.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mClient)
                .build()

        }

        fun <T> createService(context: Context, service: Class<T>): T {
            return request(context).create(service)
        }
    }

}