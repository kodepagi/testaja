package com.testaja.android.data

import com.testaja.android.data.remote.model.GenreResponse
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel


class MainRepositoryImpl(private val localRepository: MainRepository, private val remoteRepository: MainRepository) : MainRepository {

    // Remote

    override suspend fun getGenres(): GenreResponse? {
        return remoteRepository.getGenres()
    }

    override suspend fun getMovies(genre: Int): MovieResponse? {
        return remoteRepository.getMovies(genre)
    }

    override suspend fun searchMovies(query: String): MovieResponse? {
        return remoteRepository.searchMovies(query)
    }

    override suspend fun getMovieDetail(movieId: Int): MovieDetailResponse? {
        return remoteRepository.getMovieDetail(movieId)
    }

    // Local

    override suspend fun saveGenres(genre: List<GenreModel>): String {
        return localRepository.saveGenres(genre)
    }

    override suspend fun getGenresLocal(): List<GenreModel>? {
        return localRepository.getGenresLocal()
    }

    override suspend fun saveMovies(movies: List<MovieModel>): String {
        return localRepository.saveMovies(movies)
    }

    override suspend fun getMoviesLocal(genre: Int): List<MovieModel>? {
        return localRepository.getMoviesLocal(genre)
    }

    override suspend fun saveFavourite(favouriteModel: FavouriteModel): String {
        return localRepository.saveFavourite(favouriteModel)
    }

    override suspend fun getFavourites(): List<FavouriteModel>? {
        return localRepository.getFavourites()
    }

    override suspend fun deleteFavourite(movieId: Int): String {
        return localRepository.deleteFavourite(movieId)
    }


}