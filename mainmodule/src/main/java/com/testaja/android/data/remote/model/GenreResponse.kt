package com.testaja.android.data.remote.model


import com.google.gson.annotations.SerializedName
import com.testaja.db.model.GenreModel

data class GenreResponse(
    @SerializedName("genres")
    val genres: List<GenreModel>?
)