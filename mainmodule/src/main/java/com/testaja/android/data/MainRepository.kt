package com.testaja.android.data

import com.testaja.android.data.remote.model.GenreResponse
import com.testaja.android.data.remote.model.MovieDetailResponse
import com.testaja.android.data.remote.model.MovieResponse
import com.testaja.core.base.ResultSet
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel

interface MainRepository {

    // remote
    suspend fun getGenres(): GenreResponse?
    suspend fun getMovies(genre: Int): MovieResponse?
    suspend fun searchMovies(query: String): MovieResponse?
    suspend fun getMovieDetail(movieId: Int): MovieDetailResponse?

    // local
    suspend fun saveGenres(genre: List<GenreModel>): String
    suspend fun getGenresLocal(): List<GenreModel>?
    suspend fun saveMovies(movies: List<MovieModel>): String
    suspend fun getMoviesLocal(genre: Int): List<MovieModel>?
    suspend fun saveFavourite(favouriteModel: FavouriteModel): String
    suspend fun getFavourites(): List<FavouriteModel>?
    suspend fun deleteFavourite(movieId: Int): String

}