package com.testaja.android.di

import com.testaja.android.ui.detail.DetailFragment
import com.testaja.android.ui.detail.DetailModule
import com.testaja.android.ui.favourite.FavouriteFragment
import com.testaja.android.ui.favourite.FavouriteModule
import com.testaja.android.ui.main.MainFragment
import com.testaja.android.ui.main.MainModule
import com.testaja.android.ui.search.SearchFragment
import com.testaja.android.ui.search.SearchModule
import com.testaja.android.ui.splash.SplashFragment
import com.testaja.android.ui.splash.SplashModule
import dagger.Component

@Component(modules = [
    MainDataModule::class,
    MainModule::class,
    SplashModule::class,
    SearchModule::class,
    DetailModule::class,
    FavouriteModule::class])
@MainScope
interface MainComponent {

    fun inject(fragment: SplashFragment)

    fun inject(fragment: MainFragment)

    fun inject(fragment: SearchFragment)

    fun inject(fragment: DetailFragment)

    fun inject(fragment: FavouriteFragment)

}