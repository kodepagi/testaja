package com.testaja.android.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import com.testaja.core.base.BaseModule
import com.testaja.android.data.MainRepository
import com.testaja.android.data.MainRepositoryImpl
import com.testaja.android.data.remote.MainService
import com.testaja.android.data.local.MainLocalDataSource
import com.testaja.android.data.remote.MainRemoteDataSource
import com.testaja.android.domain.MainUseCase
import com.testaja.core.AppEnviroment
import com.testaja.db.AppDatabase
import kotlinx.coroutines.Dispatchers



@Module(includes = [BaseModule::class])
class MainDataModule(val mContext: Context) {


    @MainScope @Provides
    fun provideContext(): Context {
        return mContext
    }

    @MainScope @Provides
    fun provideService(@MainScope context: Context): MainService {
        return MainService.createService(context, MainService::class.java)
    }

    @MainScope @Provides
    fun provideDB(@MainScope context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, AppEnviroment.ConstOther.DB_AUTHENTICATION )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
    }

    @MainScope @Provides
    fun provideRepository(@MainScope localDataSource: MainLocalDataSource, @MainScope remoteDataSource: MainRemoteDataSource): MainRepository {
        return MainRepositoryImpl(localDataSource, remoteDataSource)
    }

    @MainScope @Provides
    fun provideRemote(@MainScope service: MainService): MainRemoteDataSource {
        return MainRemoteDataSource(service)
    }

    @MainScope @Provides
    fun provideLocal(@MainScope context: Context, appDatabase: AppDatabase): MainLocalDataSource {
        return MainLocalDataSource(context, appDatabase)
    }

    @MainScope @Provides
    fun provideUseCase(@MainScope context: Context, @MainScope repository: MainRepository): MainUseCase {
        return MainUseCase(context, repository)
    }

    @MainScope @Provides
    fun provideSchedulerProvider() = Dispatchers.Main

}