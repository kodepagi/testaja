package com.testaja.db.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "sample")
data class SampleModel(

        @PrimaryKey()
        @SerializedName("key")
        var keyId: Int,

        @SerializedName("id")
        val id: Int,

        @SerializedName("created_at")
        val createdAt: String,

        @SerializedName("date")
        val date: String,

        @SerializedName("updated_at")
        val updatedAt: String
)