package com.testaja.db.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "favourite")
data class FavouriteModel(
        @PrimaryKey()
        @SerializedName("id")
        val id: Int,

        @SerializedName("title")
        val title: String?,

        @SerializedName("poster_path")
        val posterPath: String?,

        @SerializedName("overview")
        val overview: String?,

        @SerializedName("vote_average")
        val voteAverage: Double?,

        @SerializedName("release_date")
        val releaseDate: String?
)