package com.testaja.db.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "genre")
data class GenreModel(
        @PrimaryKey()
        @SerializedName("id")
        val id: Int?,
        @SerializedName("name")
        val name: String?
)