package com.testaja.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.testaja.db.dao.FavouriteDao
import com.testaja.db.dao.GenreDao
import com.testaja.db.dao.MovieDao
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel

@Database(entities = [
    FavouriteModel::class,
    GenreModel::class,
    MovieModel::class], version = 1)
//@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun genreDao(): GenreDao

    abstract fun favouriteDao(): FavouriteDao

    abstract fun movieDao(): MovieDao

}