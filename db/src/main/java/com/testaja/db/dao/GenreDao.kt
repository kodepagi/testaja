package com.testaja.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testaja.db.model.GenreModel
import com.testaja.db.model.MovieModel
import com.testaja.db.model.SampleModel

@Dao
interface GenreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: List<GenreModel>)

    @Query("SELECT * FROM genre")
    suspend fun getData(): List<GenreModel>
}