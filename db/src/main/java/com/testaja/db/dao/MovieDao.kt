package com.testaja.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testaja.db.model.MovieModel
import com.testaja.db.model.SampleModel

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: List<MovieModel>)

    @Query("SELECT * FROM movie WHERE genreId = :genreId")
    suspend fun getData(genreId: Int): List<MovieModel>
}