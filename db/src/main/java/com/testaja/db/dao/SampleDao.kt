package com.testaja.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testaja.db.model.SampleModel

@Dao
interface SampleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: SampleModel)

    @Query("SELECT * FROM sample")
    suspend fun getData(): SampleModel?
}