package com.testaja.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.testaja.db.model.FavouriteModel
import com.testaja.db.model.MovieModel
import com.testaja.db.model.SampleModel

@Dao
interface FavouriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: FavouriteModel)

    @Query("SELECT * FROM favourite")
    suspend fun getData(): List<FavouriteModel>

    @Query("DELETE FROM favourite WHERE id = :movieId")
    suspend fun deleteData(movieId: Int)
}